package hello

import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.Decoder
import io.circe.Encoder
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody

case class HelloServiceError(msg: String)

object HelloServiceError {
  val errorMapper: EndpointOutput[HelloServiceError] =
    statusCode(StatusCode.InternalServerError) and jsonBody[HelloServiceError]

  implicit val jsonEncoder: Encoder[HelloServiceError] = deriveEncoder

  implicit val jsonDecoder: Decoder[HelloServiceError] = deriveDecoder
}
