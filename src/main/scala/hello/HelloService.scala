package hello

import zio.Task
import zio.ZIO
import zio.ZLayer

trait HelloService {
  def hello: Task[Either[HelloServiceError, String]]
}

final case class HelloServiceImpl(greeting: String) extends HelloService {
  override def hello: Task[Either[HelloServiceError, String]] = ZIO.succeed(Right(greeting))
}

object HelloService {
  val layer: ZLayer[Any, Nothing, HelloService] = ZLayer(ZIO.succeed(HelloServiceImpl("Hello, World!")))
}
