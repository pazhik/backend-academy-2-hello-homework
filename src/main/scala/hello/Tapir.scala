package hello

import zio.Runtime
import zio.Task
import zio.Unsafe
import scala.concurrent.Future

object Tapir {

  def wrap[A](action: Task[A])(implicit runtime: Runtime[Any]): Future[A] =
    Unsafe.unsafe { implicit unsafe =>
      runtime.unsafe.runToFuture(action)
    }

}
