package hello

import hello.HelloServiceError.errorMapper
import hello.Tapir.wrap
import sttp.tapir._
import sttp.tapir.json.circe.jsonBody
import zio.Runtime

class Route(helloService: HelloService)(implicit runtime: Runtime[Any]) {

  private val helloRoute = endpoint
    .in("hello")
    .out(jsonBody[String])
    .errorOut(errorMapper)
    .serverLogic(_ => wrap(helloService.hello))

  val route = List(helloRoute)

}
