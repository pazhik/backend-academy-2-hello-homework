package hello

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import hello.Tapir.wrap
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import zio.ZIO
import zio.ZIOAppDefault
import scala.concurrent.duration.Duration
import scala.concurrent.Await
import scala.concurrent.ExecutionContext

object App {

  implicit val ac: ActorSystem = ActorSystem("my-system")

  implicit val ec: ExecutionContext = ac.dispatcher

  def main(args: Array[String]): Unit =
    Await.result(wrap(Server().run)(zio.Runtime.default), Duration.Inf)

}

case class Server()(implicit ac: ActorSystem, ec: ExecutionContext) extends ZIOAppDefault {

  val server =
    for {
      runtime    <- ZIO.runtime[Any]
      hello      <- ZIO.service[HelloService]
      helloRoute  = new Route(hello)(runtime)
      tapirRoutes = AkkaHttpServerInterpreter().toRoute(helloRoute.route)
      binding <- ZIO.fromFuture { _ =>
                   Http()
                     .newServerAt("localhost", 8080)
                     .bind(tapirRoutes)
                     .andThen { case b => println(s"server started at: $b") }
                 }
    } yield binding

  override def run: ZIO[Any, Throwable, Http.ServerBinding] =
    server.provide(HelloService.layer)

}
