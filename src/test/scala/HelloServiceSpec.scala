import hello.HelloService
import zio.ZIO
import zio.test.ZIOSpecDefault
import zio.test.assertTrue

object HelloServiceSpec extends ZIOSpecDefault {

  def spec = suite("HelloSpec")(test("happy-path") {
    for {
      service  <- ZIO.service[HelloService]
      greeting <- service.hello
    } yield assertTrue(greeting == Right("Hello, World!"))
  }).provideLayer(HelloService.layer)

}
