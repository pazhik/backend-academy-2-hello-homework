ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val circeVer = "0.14.4"
lazy val tapirVer = "1.2.8"
lazy val zioVer   = "2.0.8"
lazy val scoverageVer   = "2.0.8"

lazy val root = (project in file("."))
  .settings(
    name := "backend-academy-2-hello-homework",
    libraryDependencies ++= Seq(
      "dev.zio"                     %% "zio"                              % zioVer,
      "dev.zio"                     %% "zio-test"                         % zioVer % Test,
      "dev.zio"                     %% "zio-test-magnolia"                % zioVer % Test,
      "dev.zio"                     %% "zio-test-sbt"                     % zioVer % Test,
      "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server"           % tapirVer,
      "com.softwaremill.sttp.tapir" %% "tapir-json-circe"                 % tapirVer,
      "io.circe"                    %% "circe-core"                       % circeVer,
      "io.circe"                    %% "circe-generic"                    % circeVer,
      "org.scoverage"                % "scalac-scoverage-plugin_2.13.0"   % scoverageVer,
      "org.scoverage"                % "scalac-scoverage-reporter_2.13"   % scoverageVer,
      "org.scoverage"                % "scalac-scoverage-serializer_2.13" % scoverageVer,
      "org.scoverage"                % "scalac-scoverage-domain_2.13"     % scoverageVer
    ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
  )